﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostController : MonoBehaviour {

	public float time;

	Coroutine moveCorutine;

	public void Move (float curSpeed) {
		if (moveCorutine != null)
			StopCoroutine (moveCorutine);
		moveCorutine = StartCoroutine (Moving (curSpeed));
	}

	public void Reset () {
		if (moveCorutine != null)
			StopCoroutine (moveCorutine);
		gameObject.SetActive (false);
		GameController.instance.PasteToPool (this);
	}

	IEnumerator Moving (float currentSpeed) {
		
		float currentTime = 0;

		Vector3 startPosition = transform.position;

		while (currentTime < time) {
			currentTime += Time.deltaTime;
			transform.Translate(0, Time.deltaTime * currentSpeed, 0);
			yield return null;
		}
		gameObject.SetActive (false);
		GameController.instance.PasteToPool (this);
	}

	void OnMouseDown () {
		GameManager.Scores += GameManager.scoreVal;
		Reset ();
	}
}
