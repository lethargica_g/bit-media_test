﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour {

	public GameObject itemPrefab;
	public Transform itemsHolder;

	public static Queue<GameObject> poolItems = new Queue<GameObject> ();

	void Awake () {
		CreatePool ();
	}
	
	void CreatePool () {
		poolItems = Generate (itemPrefab, itemsHolder);
	}

	Queue<GameObject> Generate (GameObject obj, Transform holder) {
		Queue<GameObject> queue = new Queue<GameObject> ();

		for (int i = 0; i < GameManager.itemsCount; i++) {
			GameObject objectToSpawn = Instantiate (obj, holder.position, Quaternion.identity) as GameObject;
			objectToSpawn.transform.SetParent (holder, false);
			objectToSpawn.SetActive (false);
			queue.Enqueue (objectToSpawn);
		}
		return queue;
	}
}
