﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	public static UnityEvent onScoreChanged = new UnityEvent ();
	public static UnityEvent onEnemyCountChanged = new UnityEvent ();
	public static UnityEvent onStartGame = new UnityEvent();
	public static UnityEvent onExitGame = new UnityEvent();
	public static UnityEvent onRestartGame = new UnityEvent ();

	public static int itemsCount = 10;
	public static int scoreVal = 10;

	static int scores;
	public static int Scores {
		get { 
			return scores;
		}
		set { 
			scores = value;
			onScoreChanged.Invoke ();
		}
	}

	static int enemyOnSceneCount;
	public static int EnemyOnSceneCount {
		get { 
			return enemyOnSceneCount;
		}
		set { 
			enemyOnSceneCount = value;
			onEnemyCountChanged.Invoke ();
		}
	}

	void Awake () {
		instance = this;
		Application.targetFrameRate = 60;
		Input.backButtonLeavesApp = true;
	}

	public void InIt () {
		onStartGame.Invoke ();
	}

	public void Exit () {
		Time.timeScale = 1;
		onExitGame.Invoke ();
	}

	public void Restart () {
		Time.timeScale = 1;
		onRestartGame.Invoke ();
	}
}
