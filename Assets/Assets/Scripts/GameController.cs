﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour {

	public static GameController instance;

	[Serializable]
	public class Count
	{
		public float minimum;
		public float maximum;

		public Count (float min, float max) {
			minimum = min; 
			maximum = max;
		}
	}

	public Count speed = new Count (2, 8);
	public Count offset = new Count (0.2f, 0.5f);
	private Count posX = new Count (-1, 1);

	private List<GhostController> ghostsOnScene = new List<GhostController> ();
	public Vector3 startPosition;

	Coroutine spawnCorutine;

	void Awake () {
		instance = this;
		GetPositionsX ();
	}

	public void Start () {
		GameManager.onExitGame.AddListener (ExitGame);
		GameManager.onStartGame.AddListener (InGame);
		GameManager.onRestartGame.AddListener (Restart);
	}

	void InGame () {
		GameManager.EnemyOnSceneCount = 0;
		GameManager.Scores = 0;
		Spawn ();
	}

	void GetPositionsX () {
		float height = 2f * Camera.main.orthographicSize;
		float width = height * Camera.main.aspect;
		posX.minimum = -width / 2 + 0.5f;
		posX.maximum = width / 2 - 0.5f;
	}
	
	void Spawn () {
		if (spawnCorutine != null)
			StopCoroutine (spawnCorutine);
		spawnCorutine = StartCoroutine (Spawning ());
	}

	IEnumerator Spawning () {

		yield return new WaitForSeconds (1.5f);

		for (int i = 0; i < GameManager.itemsCount; i++) {

			float currentSpeed = 0;

			if (GameManager.EnemyOnSceneCount > GameManager.itemsCount) {
				yield return new WaitForSeconds (5.0f);
				ExitGame ();
				yield break;
			}

			float xPos = Random.Range(posX.minimum, posX.maximum);
			Vector3 randomPosition = startPosition + new Vector3 (xPos, 0, 0);
			currentSpeed = Random.Range (speed.minimum, speed.maximum);

			GhostController ghost = PasteFromPool ();
			ghost.gameObject.transform.position = randomPosition;

			ghostsOnScene.Add (ghost);
			GameManager.EnemyOnSceneCount++;

			ghost.Move (currentSpeed);
			yield return new WaitForSeconds (Random.Range(offset.minimum, offset.maximum));
		}
	}

	GhostController PasteFromPool () {
		GameObject obj = Pool.poolItems.Dequeue ();
		obj.SetActive (true);
		GhostController ghost = obj.GetComponent<GhostController>();
		return ghost;
	}

	public void PasteToPool (GhostController ghost) {
		GameObject obj = ghost.gameObject;
		obj.SetActive (false);
		obj.transform.position = startPosition;
		Pool.poolItems.Enqueue (obj);
	}

	void ExitGame () {
		for (int i = 0; i < ghostsOnScene.Count; i++) {
			PasteToPool (ghostsOnScene [i]);
		}
		ghostsOnScene.Clear ();
		StopAllCoroutines ();
	}

	void Restart () {
		ExitGame ();
		InGame ();
	}
}
