﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCounter : MonoBehaviour {

	private Text countText;

	void Start () {
		countText = GetComponent<Text> ();
		GameManager.onEnemyCountChanged.AddListener (OnCounChanged);
	}

	void OnCounChanged () {
		countText.text = GameManager.EnemyOnSceneCount + " / " + GameManager.itemsCount;
	}
	
}
