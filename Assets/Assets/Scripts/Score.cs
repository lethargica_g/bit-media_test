﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public string prefix = "Score: ";

	private Text score;

	void Start () {
		score = GetComponent<Text> ();
		OnScoresChanged ();
		GameManager.onScoreChanged.AddListener (OnScoresChanged);
	}

	void OnScoresChanged () {
		score.text = prefix + GameManager.Scores;
	}
}
