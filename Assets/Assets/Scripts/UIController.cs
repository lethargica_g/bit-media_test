﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

	public GameObject playButton;
	public GameObject inGameUI;
	public GameObject menuButton;
	public GameObject menuPanel;

	void Start () {
		playButton.SetActive (true);
		inGameUI.SetActive (false);
		GameManager.onStartGame.AddListener (OnStartGame);
		GameManager.onExitGame.AddListener (ExitGame);
		GameManager.onRestartGame.AddListener (OnStartGame);
	}

	void OnStartGame () {
		menuPanel.SetActive (false);
		playButton.SetActive (false);
		inGameUI.SetActive (true);
		menuButton.SetActive (true);
	}

	void ExitGame () {
		menuButton.SetActive (false);
		menuPanel.SetActive (false);
		inGameUI.SetActive (false);
		playButton.SetActive (true);
	}
}
